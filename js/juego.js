"use strict";


let palabraRandom; //variable para almacenar la palabra seleccionada del Array
let nErrores = 0; //variable contador para errores
let nAciertos = 0; //variable contador para aciertos


//Array con todas las palabras
const palabras = [
    'monitor',    
    'microfono',     
    'movil',    
    'ñoquis',       
    'streamer',    
    'twitter',       
    'serpiente',   
    'programacion',     
	'parrafo',
	'javaScript',
	'pepinillo',
	'paraledepipedo',
	'pejelagarto',
    'Supercalifragilisticoexpialidoso'
];

//variables de uso global que buscan contenido en el HTML por medio de la funcion "id"
const btn = id('jugar');
const imagen = id( 'imagen');

//nos traemos la cantidad de elementos button que hay. que este caso solo hay un elemento tipo button
const btnLetras = document.querySelectorAll( "#letras button" );

//INCIIAMOS EL Parrafo resultado con "JUGAR"
id('resultado').innerHTML = "JUGAR";

//boton para iniciar el juego en evento click llama a la funcion "iniciar"
btn.addEventListener('click', iniciar );

function iniciar(){
    imagen.src = 'img/img0.png';
    //desabilitamos el boton de iniciar una ves tengamos la palabra
    btn.disabled = true;
    nErrores = 0;
    nAciertos = 0; 

    const parrafo = id( 'palabra_a_adivinar' );
    //iniciamos donde va la palabra en vacio
    parrafo.innerHTML = ''; 

    //recorremos el array para saber la cantidad de palabras que tiene 
    // y le pasamos los valores a la funcion "obtenerRandom" 0 como numMin y el lenght del array como numMax
    const nPalabras = palabras.length;
    const nRandom = obtenerRandom( 0, nPalabras );

    palabraRandom = palabras[ nRandom ];
    const nLetras = palabraRandom.length;

    id('resultado').innerHTML = "Adivina la Palabra";
console.log(palabraRandom);
    //habilitamos los botones del las letras al iniciar el jeugo
    for( let i = 0; i < btnLetras.length ; i++ ){
        btnLetras[ i ].disabled = false;
    }

    //Con este For agregas "Objetos SPAN" al parrafo palabra_a_adivinar (donde va la palabraRandom) con la funcion createElement 
    for( let i = 0; i < nLetras; i++ ){
        const span = document.createElement( 'span' );
        parrafo.appendChild( span );
    }

}

//Con esto hacemos que los botones sean interactivos y llamen a la funcion clickLetras
for( let i = 0; i < btnLetras.length ; i++ ){
    btnLetras[ i ].addEventListener( 'click', clickLetras );
}

//funcion llamada al precionar algun "span"
function clickLetras(event){
    const spans = document.querySelectorAll( '#palabra_a_adivinar span' );
    const button = event.target; //cuál de todas las letras, llamó a la función.
    button.disabled = true;

    //Aca nos traemos el contendo del boton
    const letra = button.innerHTML.toLowerCase( );
    const palabra = palabraRandom.toLowerCase( );

    let acerto = false;
    for( let i = 0; i < palabra.length;  i++ ){
        if( letra == palabra[i] ){
            //la variable i es la posición de la letra en la palabra.
            //que coincide con el span al que tenemos que mostarle esta letra...
            //innerHTML = Cambiar contenido
            spans[i].innerHTML = letra;
            nAciertos++;
            acerto = true;
        }
    }

    if( acerto == false ){
        nErrores++;
        const source = `img/img${nErrores}.png` ;
        imagen.src = source;
    }
    //comprueba cantidad de errores o aciertos y llama a la funcion "finDeJuego" para iniciar nuevamente 
    if( nErrores == 6 ){
        id('resultado').innerHTML ="Perdiste, la palabra era " + palabraRandom;
        finDeJuego( );
    }else if( nAciertos == palabraRandom.length ){
        const source = `img/win.png` ;
        imagen.src = source
        id('resultado').innerHTML = "GANASTE!";
        finDeJuego( );
    }
}

//Funcion de Fin de juego
function finDeJuego( ){
    //FOR para bloquear todas las letras una vez acabe el juego
    for( let i = 0; i < btnLetras.length ; i++ ){
        btnLetras[ i ].disabled = true;
    }

    //Habilitamos el boton para volver a iniciar
    btn.disabled = false;
}


//FUNCIONES GENERALES

//Funcion para traer contenido de HTML de manera simple llamando a la funcion "id('NOMBRE DE ID')", nos evitamos tener que llamar "document.getElementById" en todo lo que queremos buscar. 
function id( str ){
    return document.getElementById( str );
}

//Funcion para obtener un numero random y retornamos el numero.
function obtenerRandom( numMin, numMax ){
    const amplitudValores = numMax - numMin;
    const nRandom = Math.floor( Math.random( ) * amplitudValores ); //un random sin decimales de la cantidad de elementos del array de almacen de palabras
    return nRandom;
}



finDeJuego( );

/*
########  Variables y Su funcion en el codigo   ##########

btn = BOTON DONDE INICIAMOS EL JUEGO
btnLetras = TODOS LOS BOTONES DE LAS LETRAS QUE SE PUEDEN SELECCIONAR
imagen = IMAGEN DEL JUEGO 
nErrores = CONTADOR PARA ALMACENAR LA CANTIDAD DE ERRORES
nAciertos = CONTADOR PARA ALMACENAR LA CANTIDAD DE ACIERTOS
palabraRandom = PARA ALMACENAR LA PLABRA SELECCIONADA
palabras = ARREGLO CON LAS PALABRAS DISPONIBLES PARA USAR
resultado  = ESTE PARRAFO APORTA INFORMACION AL USUARIO
parrafo = ALMACENA Y ES QUIEN SE ENCARGARA DE GENERAR LOS SPAN POR CADA UNA DE LAS LETRAS DE LA PALABRA, ESTA SE INICIA CON SPAN CON VALOR "_" Y SE VA CAMBIANDO MEDIANTE ACIERTOS
nPalabras = CANTIDAD DE PALABRAS QUE DISPONE EL ARRAY
nLetras = CANTIDAD DE LETRAS QUE DISPONE LA PALABRA SELECCIONADA
nRandom = NUMERO RAMDOM GENERADO POR FUNCION obtenerRandom
span = CONVERTIR "span" COMO OBJETO PARA SU POSTERIOR USO
spans = CONTADOR DE LA CANTIDA DE OBJETOS "span" Q TIENE EL PARRAFO "palabra_a_adivinar"
button = ALMACENAR EL NUMERO DE BOTON DE LETRA QUE SE HA PRECIONADO DEL SELECTOR DE LETRAS
letra = ALMACENAR EL VALOR DEL CONTENIDO DEL BOTON DE LA LETRA SELECCIONADA  
palabra = ALMACENAR LA PALABRA EN USO
source = ALMACENAR EL NOMBRE DE LA IMAGEN A MOSTRAR
*/

